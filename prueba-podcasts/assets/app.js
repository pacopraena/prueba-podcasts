/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

import 'bootstrap-sass';

import './styles/app.scss';

import './bootstrap';
import 'bootstrap/dist/js/bootstrap.bundle';

// --- \\

function onlyPlayOneIn(container) {
    container.addEventListener("play", function(event) {
        const audio_elements = container.querySelectorAll("audio");
        for(let i=0; i < audio_elements.length; i++) {
            const audio_element = audio_elements[i];
            if (audio_element !== event.target) {
                audio_element.pause();
            }
        }
    }, true);
}

document.addEventListener("DOMContentLoaded", function() {
    onlyPlayOneIn(document.body);
});