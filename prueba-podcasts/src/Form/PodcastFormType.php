<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PodcastFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'Título',
            'attr' => [
                'placeholder' => 'Título'
            ]
        ]);

        $builder->add('description', TextareaType::class, [
            'label' => 'Descripción',
            'attr' => [
                'placeholder' => 'Escribe aquí la descripción de tu podcast...'
            ]
        ]);

        $builder->add('audio', FileType::class, [
            'label' => 'Sube tu podcast',
            'mapped' => false,
            'required' => false,
            'attr' => [
                'placeholder' => 'Sube tu podcast'
            ],
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'audio/x-wav',
                        'audio/mpeg',
                        'audio/x-aiff',
                        'audio/ogg'
                    ],
                    'mimeTypesMessage' => 'Por favor, suba un audio con formato wav, mp3 o aiff.'
                ])
            ]
        ]);

        $builder->add('image', FileType::class, [
            'label' => 'Imagen',
            'mapped' => false,
            'required' => false,
            'attr' => [
                'placeholder' => 'Imagen'
            ],
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/gif',
                        'image/png',
                        'image/jpeg'
                    ],
                    'mimeTypesMessage' => 'Por favor, suba una imagen válida.'
                ])
            ]
        ]);

    }
}
