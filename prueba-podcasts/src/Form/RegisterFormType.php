<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegisterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'Nombre',
            'attr' => [
                'placeholder' => 'Nombre'
            ]
        ]);

        $builder->add('surname', TextType::class, [
            'label' => 'Apellidos',
            'attr' => [
                'placeholder' => 'Apellidos'
            ]
        ]);

        $builder->add('email', EmailType::class, [
            'label' => 'Correo electrónico',
            'attr' => [
                'placeholder' => 'Correo electrónico'
            ]
        ]);

        $builder->add('password', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'Las contraseñas no coinciden',
            'options' => ['attr' => ['class' => 'password-field']],
            'required' => true,
            'first_options' => [
                'label' => 'Contraseña',
                'attr' => [
                    'placeholder' => 'Contraseña'
                ]
            ],
            'second_options' => [
                'label' => 'Confirmar contraseña',
                'attr' => [
                    'placeholder' => 'Confirmar contraseña'
                ]
                ]
        ]);

        $builder->add('image', FileType::class, [
            'label' => 'Imagen de perfil',
            'mapped' => false,
            'required' => false,
            'attr' => [
                'placeholder' => 'Imagen de perfil'
            ],
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/gif',
                        'image/png',
                        'image/jpeg'
                    ],
                    'mimeTypesMessage' => 'Por favor, suba una imagen válida.'
                ])
            ]
        ]);

    }
}
