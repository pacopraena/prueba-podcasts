<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterFormType;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\User\UserInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder, FileUploader $fileUploader): Response
    {
        $user = new User();
        $form = $this->createForm(RegisterFormType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $user->setRole('ROLE_USER');

            $encodedPassword = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encodedPassword);

            $user->setCreatedAt(new \DateTime('now'));

            $imageFile = $form->get('image')->getData();

            if($imageFile){
                $fileUploader->setDirectory('/profile_images');
                $imageFileName = $fileUploader->upload($imageFile);
                $user->setImageFilename($imageFileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('login');
        }
        return $this->render('user/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/", name="login")
     *
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('user/login.html.twig', [
            'error' => $error,
            'last_username' => $lastUsername
        ]);
    }

    /**
     * @Route("/users/edit-user/{id}", name="user_edit")
     */
    public function edit(Request $request, UserPasswordEncoderInterface $encoder, UserInterface $userLogged, User $user): Response
    {
        if(!$userLogged || ($userLogged->getRole() != 'ROLE_ADMIN' && $userLogged->getId() != $user->getId())){
            return $this->redirectToRoute('podcasts');
        }

        $form = $this->createForm(RegisterFormType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $encodedPassword = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encodedPassword);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            if($userLogged->getRole() == 'ROLE_ADMIN'){
                return $this->redirectToRoute('table_users');
            }else{
                return $this->redirectToRoute('podcasts');
            }
        }

        return $this->render('user/register.html.twig', [
            'edit' => true,
            'user' => $user,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function adminIndex(UserInterface $userLogged)
    {
        if($userLogged->getRole() == 'ROLE_ADMIN'){
            return $this->render('admin/admin-index.html.twig');
        }else{
            return $this->redirectToRoute('podcasts');
        }
    }

    /**
     * @Route("/admin/table-users", name="table_users")
     */
    public function tableUsers()
    {
        $users_repo = $this->getDoctrine()->getRepository(User::class);
        $users = $users_repo->findBy([], ['id' => 'DESC']);

        return $this->render('admin/users-table.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/admin/delete-user/{id}", name="user_delete")
     */
    public function delete(User $user, UserInterface $userLogged, FileUploader $fileUploader)
    {


        if($userLogged->getRole() != 'ROLE_ADMIN'){
            return $this->redirectToRoute('podcasts');
        }

        if(!$user){
            return $this->redirectToRoute('table_users');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('table_users');
    }

}
