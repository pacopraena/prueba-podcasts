<?php

namespace App\Controller;

use App\Entity\Podcast;
use App\Form\PodcastFormType;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class PodcastController extends AbstractController
{
    /**
     * @Route("/podcasts/home", name="podcasts")
     */
    public function index(): Response
    {
        $podcasts_repo = $this->getDoctrine()->getRepository(Podcast::class);
        $podcasts = $podcasts_repo->findBy([], ['id' => 'DESC']);

        return $this->render('podcast/podcasts.html.twig', [
            'podcasts' => $podcasts,
            'home' => true
        ]);
    }

    /**
     *@Route("/podcasts/create-podcast", name="podcast_create")
     */
    public function createPodcast(Request $request, UserInterface $user, FileUploader $fileUploader)
    {
        $podcast = new Podcast();
        $form = $this->createForm(PodcastFormType::class, $podcast);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $podcast->setCreatedAt(new \DateTime('now'));
            $podcast->setUser($user);

            $imageFile = $form->get('image')->getData();
            $audioFile = $form->get('audio')->getData();

            if($imageFile){
                $fileUploader->setDirectory('/podcasts_images');
                $imageFileName = $fileUploader->upload($imageFile);
                $podcast->setImageFilename($imageFileName);
            }

            if($audioFile){
                if($imageFile){
                    $fileUploader->setDirectory('/../podcasts_audios');
                }else{
                    $fileUploader->setDirectory('/podcasts_audios');
                }

                $audioFileName = $fileUploader->upload($audioFile);
                $podcast->setAudioFilename($audioFileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($podcast);
            $em->flush();

            return $this->redirect(
                $this->generateUrl('my_podcasts')
            );
        }

        return $this->render('podcast/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/podcasts/my-podcasts", name="my_podcasts")
     */
    public function myPodcasts(UserInterface $user)
    {
        $podcasts = $user->getPodcasts();

        return $this->render('podcast/my-podcasts.html.twig', [
            'podcasts' => $podcasts
        ]);
    }

    /**
     * @Route("/podcasts/edit-podcast/{id}", name="podcast_edit")
     */
    public function edit(Request $request, UserInterface $user, Podcast $podcast, FileUploader $fileUploader): Response
    {
        if(!$user || ($user->getRole() != 'ROLE_ADMIN' && $user->getId() != $podcast->getUser()->getId())) {
            return $this->redirectToRoute('my_podcasts');
        }

        $form = $this->createForm(PodcastFormType::class, $podcast);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $imageFile = $form->get('image')->getData();
            $audioFile = $form->get('audio')->getData();

            if($imageFile){
                $fileUploader->setDirectory('/podcasts_images');
                $imageFileName = $fileUploader->upload($imageFile);
                $podcast->setImageFilename($imageFileName);
            }

            if($audioFile){

                if($imageFile){
                    $fileUploader->setDirectory('/../podcasts_audios');
                }else{
                    $fileUploader->setDirectory('/podcasts_audios');
                }

                $audioFileName = $fileUploader->upload($audioFile);
                $podcast->setAudioFilename($audioFileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($podcast);
            $em->flush();

            if($user->getRole() == 'ROLE_ADMIN'){
                return $this->redirectToRoute('table_podcasts');
            }else{
                return $this->redirectToRoute('my_podcasts');
            }
        }

        return $this->render('podcast/create.html.twig', [
            'edit' => true,
            'podcast' => $podcast,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/podcasts/delete-podcast/{id}", name="podcast_delete")
     */
    public function delete(Podcast $podcast, UserInterface $user, FileUploader $fileUploader)
    {


        if( !$user || ($user->getRole() != 'ROLE_ADMIN' && $user->getId() != $podcast->getUser()->getId())){
            return $this->redirectToRoute('my_podcasts');
        }

        if(!$podcast){
            return $this->redirectToRoute('my_podcasts');
        }

        $filesystem = new Filesystem();
        $filesystem->remove($fileUploader->getTargetDirectory().'podcasts_audios/'.$podcast->getAudioFilename());
        if($podcast->getImageFilename()){
            $filesystem->remove($fileUploader->getTargetDirectory().'podcasts_images/'.$podcast->getImageFilename());
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($podcast);
        $em->flush();

        if($user->getRole() == 'ROLE_ADMIN'){
            return $this->redirectToRoute('table_podcasts');
        }else{
            return $this->redirectToRoute('my_podcasts');
        }

    }

    /**
     * @Route("/admin/table-podcasts", name="table_podcasts")
     */
    public function tablePodcasts()
    {
        $podcasts_repo = $this->getDoctrine()->getRepository(Podcast::class);
        $podcasts = $podcasts_repo->findBy([], ['id' => 'DESC']);

        return $this->render('admin/podcasts-table.html.twig', [
            'podcasts' => $podcasts
        ]);
    }
}
