CREATE DATABASE IF NOT EXISTS podcasts;
USE podcasts;

CREATE TABLE IF NOT EXISTS users(
    id                      int(255) auto_increment not null,
    role                    varchar(50) not null,
    name                    varchar(100),
    surname                 varchar(200),
    image_filename          varchar(255),
    email                   varchar (255) not null unique,
    password                varchar(255) not null,
    created_at              datetime,
    CONSTRAINT  pk_users PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE IF NOT EXISTS podcasts(
    id                      int(255) auto_increment not null,
    user_id                 int(255) not null,
    title                   varchar(255) not null,
    description             text,
    audio_filename          varchar (255) not null,
    image_filename          varchar(255),
    created_at              datetime,
    CONSTRAINT  pk_podcasts PRIMARY KEY(id),
    CONSTRAINT fk_podcasts_user FOREIGN KEY(user_id) REFERENCES users(id)
)ENGINE=InnoDb;